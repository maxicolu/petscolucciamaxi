//
//  ValueTableViewCell.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/27/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class ValueTableViewCell: UITableViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
