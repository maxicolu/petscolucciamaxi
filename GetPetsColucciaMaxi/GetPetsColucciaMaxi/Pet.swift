//
//  Pet.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import ObjectMapper
/*
 JSON: (
 {
    category =         {
        id = 0;
        name = string;
    };
    id = 232345234523452345;
    name = doggiestyle;
    photoUrls =         (
        string
    );
    status = available;
    tags =         (
    {
        id = 0;
        name = string;
    }
    );
 }
 */

class Pet: Mappable
{
    var id:NSNumber?
    var name:String?
    var status:String?
    var category:CategoryObject?
    var photoURLs:Array<String>?
    var tagObjects:Array<TagObject>?
    
    required init?(map: Map)
    {
        
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        status <- map["status"]
        category <- map["category"]
        photoURLs <- map["photoUrls"]
        tagObjects <- map["tags"]
    }
    
}
