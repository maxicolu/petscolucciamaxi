//
//  LibraryAPI.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class LibraryAPI: AnyObject
{
    static let sharedInstance = LibraryAPI()
    
    private let URLBase = "http://petstore.swagger.io/v2/pet"
    private let petsFind = "/findByStatus?status=available"
    
    let internetError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Please check your internet connection or try again later"])
    
    func getPets( completion:@escaping ((Array<Pet>?,Error?) -> ()) )
    {
        guard (UIApplication.shared.delegate as! AppDelegate).networkManager?.isReachable == true else
        {
            completion(nil,internetError)
            return
        }
        
        Alamofire.request(URLBase + petsFind).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseArray { (response: DataResponse<[Pet]>) in
            
            switch response.result
            {
                case .success:
                    let pets = response.result.value
                    completion(pets,nil)
                break
                case .failure(let error):
                    completion(nil,error)
                break
            }
            
            
        }
        
    }
    
    func getPet(idPet:NSNumber, completion:@escaping ((Pet?,Error?) -> ()) )
    {
        Alamofire.request(URLBase + "/" + idPet.stringValue).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseObject { (response: DataResponse<Pet>) in
           
            switch response.result
            {
            case .success:
                let pet = response.result.value
                completion(pet,nil)
                break
            case .failure(let error):
                completion(nil,error)
                break
            }
           
        }
    }
    
}
