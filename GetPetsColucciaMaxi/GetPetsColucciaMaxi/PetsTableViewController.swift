//
//  PetsTableViewController.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class PetsTableViewController: BaseTableViewController {

    private var pets:Array<Pet>?
    private var petSelected:Pet!
    
    func showAlertError(error:Error)
    {
        let alert = UIAlertController.errorAlert(message: error.localizedDescription)
        self.present(alert, animated: true, completion: nil)
    }
    
    func reloadPets()
    {
        LibraryAPI.sharedInstance.getPets { (petsArray:Array<Pet>?, error:Error?) in
            
            DispatchQueue.main.async
            {
                    self.hideLoadingView()
                    self.refreshControl?.endRefreshing()
                
                    if let error = error
                    {
                       self.showAlertError(error: error)
                    }
                
                    self.pets = petsArray
                    self.tableView.reloadData()
            }
        }
    }
    
    func addRefreshControl()
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(reloadPets), for: UIControlEvents.valueChanged)
    }
    
    //MARK: Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        self.addRefreshControl()
        self.showLoadingView()
        self.reloadPets()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if  let pets = pets,pets.count > 0
        {
            self.tableView.separatorStyle = .singleLine
            self.tableView.backgroundView = nil
            
            return 1
        }
        else
        {
            self.tableView.separatorStyle = .none
            
            let noElementsLabel = UILabel()
            noElementsLabel.backgroundColor = UIColor.white
            noElementsLabel.textColor = UIColor.black
            noElementsLabel.text = "No data is currently available. Please pull down to refresh."
            noElementsLabel.textAlignment = .center
            noElementsLabel.font = UIFont.italicSystemFont(ofSize: 20.0)
            noElementsLabel.numberOfLines = 0
            noElementsLabel.sizeToFit()
            
            self.tableView.backgroundView = noElementsLabel
            
            return 0
        }

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let pets = pets else
        {
            return 0
        }
        
        return pets.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NameCellReuseIdentifier", for: indexPath) as! NameTableViewCell

        // Configure the cell...
        let pet = pets![indexPath.row]
        
        cell.nameLabel.text = pet.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         petSelected = pets![indexPath.row]
        
        self.showLoadingView()
        LibraryAPI.sharedInstance.getPet(idPet: petSelected.id!) { (pet:Pet?,error: Error?) in
            
            DispatchQueue.main.async
            {
                self.hideLoadingView()
                
                if let pet = pet
                {
                    if (pet.photoURLs) != nil
                    {
                        self.performSegue(withIdentifier: "ShowPetAndURLsSegueIdentifier", sender: nil)
                    }
                    else
                    {
                        self.performSegue(withIdentifier: "ShowOnlyPetInfoSegueIdentifier", sender: nil)
                    }
                }
                else
                {
                    if let error = error
                    {
                        self.showAlertError(error: error)
                    }
                }
                
            }
            
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let identifier = segue.identifier
        {
            switch identifier
            {
                case "ShowOnlyPetInfoSegueIdentifier":
                    (segue.destination as! DetailPetTableViewController).pet = petSelected
                break
            
                case "ShowPetAndURLsSegueIdentifier":
                    (segue.destination as! SegmentedPetViewController).pet = petSelected
                break
            
                default:
                break
            }
        }
        
        
        
        
    }
    

}
