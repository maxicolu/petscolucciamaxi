//
//  ExtensionAlert.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/27/17.
//  Copyright © 2017 Max. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController
{
    class func errorAlert(message:String) -> UIAlertController
    {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        return alert
    }
    
    class func popUpAlert(message:String) -> UIAlertController
    {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        return alert
    }
}
