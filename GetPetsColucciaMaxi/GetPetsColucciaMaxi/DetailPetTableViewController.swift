//
//  DetailPetTableViewController.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class DetailPetTableViewController: UITableViewController {

    var pet:Pet!
    
    private let idNameSection = "Id"
    private let nameSection = "Name"
    private let statusNameSection = "Status"
    private let categoryNameSection = "Category"
    private let tagsNameSection = "Tags"
    
    private var sectionTitles = Array<String>()
    private var valueForSections = Dictionary<String,Any>()
    
    func processPet()
    {
        var tagsString = Array<String>()
        
        sectionTitles.removeAll()
        valueForSections.removeAll()
        
        sectionTitles.append(idNameSection)
        sectionTitles.append(nameSection)
        sectionTitles.append(statusNameSection)
        sectionTitles.append(categoryNameSection)
        
        valueForSections[idNameSection] = pet.id?.stringValue
        valueForSections[nameSection] = pet.name
        valueForSections[statusNameSection] = pet.status
        
        var categoryIdValue = "Id: "
        if let idCategory = pet.category?.id?.stringValue
        {
            categoryIdValue += idCategory
        }
        var categoryNameValue = "Name: "
        if let categoryName = pet.category?.name
        {
            categoryNameValue += categoryName
        }
        valueForSections[categoryNameSection] = [categoryIdValue,categoryNameValue]
        
        if let tagObjects = pet.tagObjects
        {
            for tagObj in tagObjects
            {
                var tagIdValue = "Id: "
                if let idTag = tagObj.id?.stringValue
                {
                    tagIdValue += idTag
                }
                var tagNameValue = "Name: "
                if let tagName = tagObj.name
                {
                    tagNameValue += tagName
                }
                tagsString.append(tagIdValue)
                tagsString.append(tagNameValue)
            }
        }
        
        if tagsString.count > 0
        {
            sectionTitles.append(tagsNameSection)
            valueForSections[tagsNameSection] = tagsString
        }
        
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        self.processPet()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sectionTitles.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sectionName = sectionTitles[section]
        let value = valueForSections[sectionName]
        if let array = value as? Array<String>
        {
            return array.count
        }
        else
        {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return sectionTitles[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValueCellReusableIdentifier", for: indexPath) as! ValueTableViewCell

        // Configure the cell...
        let sectionName = sectionTitles[indexPath.section]
        let value = valueForSections[sectionName]
        
        if let array = value as? Array<String>
        {
            cell.valueLabel.text = array[indexPath.row]
        }
        else
        {
            cell.valueLabel.text = value as? String
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let sectionName = sectionTitles[indexPath.section]
        let value = valueForSections[sectionName]
        
        var popUpText:String
        if let array = value as? Array<String>
        {
            popUpText = array[indexPath.row]
        }
        else
        {
            popUpText = sectionName + ": "
            if let valueString = value as? String
            {
                popUpText += valueString
            }
        }
        
        let alert = UIAlertController.popUpAlert(message: popUpText)
        self.present(alert, animated: true, completion: nil)
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
