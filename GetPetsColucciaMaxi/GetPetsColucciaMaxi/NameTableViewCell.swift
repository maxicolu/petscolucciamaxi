//
//  NameTableViewCell.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class NameTableViewCell: UITableViewCell
{

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
