//
//  Tag.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/26/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import ObjectMapper

class TagObject: Mappable
{
    var id:NSNumber?
    var name:String?
    
    required init?(map: Map)
    {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
