//
//  SegmentedPetViewController.swift
//  GetPetsColucciaMaxi
//
//  Created by User on 6/27/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class SegmentedPetViewController: UIViewController {
    
    let segmentedControl = UISegmentedControl(items: ["Info","URLs"])
    var pet:Pet!
    
    private lazy var detailPetViewController: DetailPetTableViewController = {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "DetailPetController") as! DetailPetTableViewController
        
        return viewController
    }()
    
    private lazy var URLsViewController: URLsTableViewController = {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "URLsController") as! URLsTableViewController
        
        return viewController
    }()
    
    func configNavigationItem()
    {
        self.navigationItem.titleView = segmentedControl
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(valueSegmentChanged), for: .valueChanged)
    }
    
    func add(childViewController:UIViewController)
    {
        self.addChildViewController(childViewController)
        self.view.addSubview(childViewController.view)
        childViewController.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        childViewController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        childViewController.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        childViewController.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        childViewController.didMove(toParentViewController: self)
    }
    
    func remove(childViewController:UIViewController)
    {
        childViewController.willMove(toParentViewController: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParentViewController()
    }
    
    func updateView()
    {
        switch segmentedControl.selectedSegmentIndex
        {
            case 0:
                self.remove(childViewController: URLsViewController)
                detailPetViewController.pet = pet
                self.add(childViewController: detailPetViewController)
            break
            
            case 1:
                self.remove(childViewController: detailPetViewController)
                URLsViewController.photosURLS = pet.photoURLs!
                self.add(childViewController: URLsViewController)
            break
            
            default:
            break
        }
    }
    
    //MARK: - Segment Action
    func valueSegmentChanged(sender: UISegmentedControl)
    {
        self.updateView()
    }

    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.configNavigationItem()
        self.updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func segmentValueChangedAction(_ sender: Any) {
    }
}
